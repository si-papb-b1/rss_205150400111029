package com.example.rss_205150400111029;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.widget.TextView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class MediumProgrammingAdapter extends RecyclerView.Adapter<MediumProgrammingAdapter.MediumProgrammingViewHolder>{
    LayoutInflater inflater;
    Context _context;
    List<String> data;

    public MediumProgrammingAdapter(Context _context, List<String> data){
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
        System.out.println("This is running");
    }



    @NonNull
    @Override
    public MediumProgrammingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =inflater.inflate(R.layout.row, parent, false);
        return new MediumProgrammingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MediumProgrammingViewHolder holder, int position) {
        holder.tvTitle.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MediumProgrammingViewHolder extends RecyclerView.ViewHolder{
    TextView tvTitle;
    TextView tvWriter;
    ConstraintLayout layout;

        public MediumProgrammingViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvWriter = itemView.findViewById(R.id.tvWriter);
            layout = itemView.findViewById(R.id.linearLayout);
        }
    }
}
