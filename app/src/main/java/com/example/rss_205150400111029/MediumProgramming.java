package com.example.rss_205150400111029;

public class MediumProgramming {
    private String title;
    private String writer;
    private String link;

    public String getTitle() {
        return title;
    }

    public String getWriter() {
        return writer;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }
}
