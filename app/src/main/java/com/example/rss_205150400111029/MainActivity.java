package com.example.rss_205150400111029;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.AsyncTask;
import android.util.Log;


import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    MediumProgrammingAdapter adapter;
    MainActivity local;
    public static String TESTER = "RAHASIA";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerview);

        local = this;


        Task task = new Task();

        task.execute("https://medium.com/feed/tag/programming");

        recyclerView.setLayoutManager(new LinearLayoutManager(local));
    }


    public class Task extends AsyncTask<String, Void, List<String>> {


        @Override
        protected List<String> doInBackground(String... strings) {
            try{
                RssParser rssParser = new RssParser();
                List<String> lists = rssParser.parseRssFromUrl(rssParser.loadRssFromUrl(strings[0]));
                return lists;
            } catch (Exception e){
                Log.e("RssParser", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<String> strings) {

            super.onPostExecute(strings);
            for(String i : strings){

                Log.d("Title, throw something:", i);
            }



            adapter = new MediumProgrammingAdapter(local, strings);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();



        }

    }

}